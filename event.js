/**
 * Class: Event
 *
 * @package Event
 * 
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 * @license GNU GPL 3.0 http://www.gnu.org/licenses/gpl.txt
 */

function ksort(w) {
	var sArr = [], tArr = [], n = 0, ret = {};
	for (i in w){
		tArr[n++] = i;
	}
	tArr = tArr.sort();
  for(i in tArr){
    ret[tArr[i]] = w[tArr[i]];
  }

  return ret;
}

function _event(){
  //constructor
}

//class Event

_event.prototype.low_priority     = 100;
_event.prototype.normal_priority  = 50;
_event.prototype.high_priority    = 10;
_event.prototype.express_priority = 0;
_event.prototype._events          = {};

_event.prototype.bind             = function(key, callback, context, priority){
  var ret = false;
  if ('undefined' == typeof priority) priority = this.normal_priority;
  if (('string' == typeof key) && ('function' == typeof callback) && (context instanceof _model)){
    if ('undefined' == typeof  this._events[key])
       this._events[key]           = {};
    if ('undefined' != typeof  this._events[key][priority]){
      len =  this._events[key][priority].length;
       this._events[key][priority][len] = [callback,context];
    }
    else  this._events[key][priority] = [[callback,context]];
     this._events[key] = ksort( this._events[key]);
    ret = true;
  }

  return ret;
}

_event.prototype.trigger          = function(key){
  var ret = false;
  var k   = key;
  if ('undefined' != typeof  this._events[k]){
    Array.prototype.shift.call(arguments);
    for(var i in  this._events[k]){
      for(var j in  this._events[k][i]){
        var cb =  this._events[k][i][j];
        cb[0].apply(cb[1],arguments);
      }
    }
    ret = true;
  }

  return ret;
}

var Event = new _event;

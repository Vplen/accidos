/**
 * Class: View
 *
 * @package View
 *
 * 
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 * @license GNU GPL 3.0 http://www.gnu.org/licenses/gpl.txt
 *
 */
function _view(name){
  this.set_filename(name);
}

_view.prototype.filename    = '';
_view.prototype.vars        = {};

_view.prototype.set_filename  = function(name){
  this.filename = name;
}

_view.prototype.get_filename  = function(){
  return this.filename;
}

_view.prototype.set           = function (key, value){
  this.vars[key] = value;
};

_view.prototype.get           = function (key){
  return this.vars[key];
};

_view.prototype.isset         = function (key){
  key = key.toLowerCase().replace( this.prefix_vars, '' );
  return ('undefined' != typeof this.vars[key]);
};

_view.prototype.render        = function(file){
  if ('undefined' != typeof file)
    this.set_filename(file);
  else file = this.get_filename();
  var src   = source('views/' + file);
  var F     = new Function('view', src);
  return F(this);
};

_view.prototype.factory       = function(name){
  var F                       = new Function('name','this.constructor(name);');
  F.prototype                 = _view.prototype;
  F.prototype.constructor     = _view;
  F.prototype.ancestor        = _view.prototype;
  F.prototype.vars            = {};
  F.prototype.filename        = '';
  return new F(name);
}

var View = new _view;;

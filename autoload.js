var _request = function(){
  this.init();
};

_request.prototype.request = null;

_request.prototype.init = function(){
  if(typeof ActiveXObject == 'undefined'){
      this.request = new XMLHttpRequest();
  }else{
      this.request = new ActiveXObject("Microsoft.XMLHTTP");
  }
};

_request.prototype.getSync = function(url){  
  this.request.open('GET', url, false);
  this.request.send(null);
  if (4 == this.request.readyState && 200 == this.request.status){
      return this.request.responseText;
  }else{
      return false;
  }
};

_request.prototype.getAsync = function(url, callBack){   
  this.request.open('GET', url, true);
  this.request.send(null);
  var req = this.request;
  this.request.onreadystatechange = function() {
      if (4 == req.readyState && 200 == req.status){          
          callBack(req.responseText);
      }
  }
};

var _loaderStatus = function(callback){
  this.callbacks = [];
  this.completed = 0;
  this.addCallback(callback);
};

_loaderStatus.prototype.callback = function(){
  n = this.callbacks.length;
  for (var i in this.callbacks){
      this.callbacks[i]();
      this.callbacks[i] = null;
  }
  this.completed = 1;
};

_loaderStatus.prototype.addCallback = function(callback){
  if ('function' == typeof callback){
      n = this.callbacks.length;
      this.callbacks[n] = callback;
  }
}
 
_loaderStatus.prototype.runFunction = function(callback){
  if ('function' == typeof callback){
      callback();
  }
}

var Autoloader = function(){
  var _autoLoader = function(){};

  var files     =  new Array();

  var sources   = new Array();

  var root_path = _root_core_path.path;

  var app_path  = _root_core_path.app;

  var ext       = '.js';

  _autoLoader.prototype.append_script = function(data, context){
    if ( 'undefined' == typeof context || null == context )
      context = document;
    var head = context.getElementsByTagName('head');
    if (head && head[0]){
      var script = context.createElement('script');
      var text = context.createTextNode(data);
      script.type= 'text/javascript'
      try{
        //Во всех нормальных брауцзерах 
        //это работает. Но только не в IE.
        //Безопасность знаете ли.
        script.appendChild(text);
      }catch ($e){
        //Зато в IE можно сделать вот так.
        //Работает даже в 6-й версии.
        //Интересно... И какая польза от такой защиты?..
        script.text = data;
      }
      head[0].appendChild(script);
    }
    return this;
  };

  /**
  * Этот метод загружает скрипт с сервера
  * @protected
  */
  var load = function(url, async, callback){
    var request = new _request();
    //синхронная загрузка
    if (typeof async == 'undefined' || ! async){
      if (data = request.getSync(url)){
        
        this.append_script(data);
        if ('function' == typeof callback){
          callback();
        }
        return true;
      }else{
        
        return false;
      }   
    //асинхронная загрузка
    }else{
      request.getAsync(url, 
        function(data){
          this.append_script(data);
          if ('function' == typeof callback){
            callback();
          }
        }
      );
      return true;
    }
  };

  var core_url  = function(uri){
    var m = uri.match(/http.*/);
    if ( 'undefined' == m || null == m || 0 == m.length )
      return root_path + uri.toLowerCase().replace('_', '/') + ext;
    else return uri;
  }

  var app_url   = function(uri){
    var m = uri.match(/http.*/);
    if ( 'undefined' == m || null == m || 0 == m.length )
      return app_path + uri.toLowerCase().replace('_', '/') + ext;
    else return uri;
  }

  /**
   * Этот метод подключает файл (аналогично функциям include и require в PHP)
   * @var string fileName
   * @var bool async
   * @var function callback
  */
  _autoLoader.prototype.include = function(fileName, async, callback){
    files[fileName] = new _loaderStatus(callback);
    return load.call(this, core_url(fileName), async, function(){files[fileName].callback();});
  };

  /**
   * Этот метод подключает файл единожды (аналогично функциям include_once и require_once в PHP)
   * @var string fileName
   * @var bool async
   * @var function callback
  */
  _autoLoader.prototype.includeOnce = function(fileName, async, callback){
    //Файл ранее не подключался
    if ('undefined' == files[fileName] || !files[fileName]){
      files[fileName] = new _loaderStatus(callback);           
      return load.call(this, core_url(fileName), async, function(){files[fileName].callback();});
    //Файл уже подключен, либо подключается в данный момент
    }else{
      if (files[fileName].completed){
        files[fileName].runFunction(callback);
      }else{
        files[fileName].addCallback(callback);
      }
      return true;
    }
  };

  _autoLoader.prototype.include_module = function(fileName, async, callback){
    files[fileName] = new _loaderStatus(callback);
    return load.call(this, app_url(fileName), async, function(){files[fileName].callback();});
  };

  _autoLoader.prototype.includeOnce_module = function(fileName, async, callback){
    //Файл ранее не подключался
    if ('undefined' == files[fileName] || !files[fileName]){
      files[fileName] = new _loaderStatus(callback);           
      return load.call(this, app_url(fileName), async, function(){files[fileName].callback();});
    //Файл уже подключен, либо подключается в данный момент
    }else{
      if (files[fileName].completed){
        files[fileName].runFunction(callback);
      }else{
        files[fileName].addCallback(callback);
      }
      return true;
    }
  };

  _autoLoader.prototype.source            = function(fileName, async, callback){
    var request = new _request();
    if (typeof async == 'undefined' || ! async){
      if ('undefined' != typeof sources[fileName] && sources[fileName]){
        return sources[fileName];
      }
      sources[fileName] = request.getSync(core_url(fileName));
      return sources[fileName];
    //асинхронная загрузка
    }else{
      if ('undefined' != typeof sources[fileName] && sources[fileName]){
        callback(sources[fileName]);
      }
      request.getAsync(core_url(fileName), function(src){
        sources[fileName] = src;
        callback(src);
      });
      return true;
    }
  };

  _autoLoader.prototype.source_module     = function(fileName, async, callback){
    var request = new _request();
    if (typeof async == 'undefined' || ! async){
      if ('undefined' != typeof sources[fileName] && sources[fileName]){
        return sources[fileName];
      }
      sources[fileName] = request.getSync(app_url(fileName));
      return sources[fileName];
    //асинхронная загрузка
    }else{
      if ('undefined' != typeof sources[fileName] && sources[fileName]){
        callback(sources[fileName]);
      }
      request.getAsync(app_url(fileName), function(src){
        sources[fileName] = src;
        callback(src);
      });
      return true;
    }
  };
   
  return new _autoLoader();
}();

function include(file){
  if (!Autoloader.include(file))
    Autoloader.include_module(file);
}

function include_once(file){
  if (!Autoloader.includeOnce(file))
    Autoloader.includeOnce_module(file);
}

function source(file){
  return Autoloader.source_module(file);
}

/*
  onDomReady, Copyright © 2010 Jakob Mattsson

  This is a small implementation of an onDomReady-function, for situations when frameworks are no-no.
  It's loosely based on jQuery's implementation of $.ready, Copyright (c) 2010 John Resig, http://jquery.com/
*/

(function() {

  var onDomReadyIdentifier = 'onDomReady';
  var isBound = false;
  var readyList = [];

  if (window[onDomReadyIdentifier] && typeof window[onDomReadyIdentifier] == 'function') {
    return;
  }

  var whenReady = function() {
    // Make sure body exists, at least, in case IE gets a little overzealous.
    // This is taked directly from jQuery's implementation.
    if (!document.body) {
      return setTimeout(whenReady, 13);
    }

    for (var i=0; i<readyList.length; i++) {
      readyList[i]();
    }
    readyList = [];
  };

  var bindReady = function() {
    // Mozilla, Opera and webkit nightlies currently support this event
    if (document.addEventListener) {
      var DOMContentLoaded = function() {
        document.removeEventListener("DOMContentLoaded", DOMContentLoaded, false);
        whenReady();
      };
      
      document.addEventListener("DOMContentLoaded", DOMContentLoaded, false);
      window.addEventListener("load", whenReady, false); // fallback
      
      // If IE event model is used
    } else if (document.attachEvent) {
      
      var onreadystatechange = function() {
        if (document.readyState === "complete") {
          document.detachEvent("onreadystatechange", onreadystatechange);
          whenReady();
        }
      };
      
      document.attachEvent("onreadystatechange", onreadystatechange);
      window.attachEvent("onload", whenReady); // fallback

      // If IE and not a frame, continually check to see if the document is ready
      var toplevel = false;

      try {
        toplevel = window.frameElement == null;
      } catch(e) {}

      // The DOM ready check for Internet Explorer
      if (document.documentElement.doScroll && toplevel) {
        var doScrollCheck = function() {

          // stop searching if we have no functions to call 
          // (or, in other words, if they have already been called)
          if (readyList.length == 0) {
            return;
          }

          try {
            // If IE is used, use the trick by Diego Perini
            // http://javascript.nwbox.com/IEContentLoaded/
            document.documentElement.doScroll("left");
          } catch(e) {
            setTimeout(doScrollCheck, 1);
            return;
          }

          // and execute any waiting functions
          whenReady();
        }  
        doScrollCheck();
      }
    } 
  };

  window[onDomReadyIdentifier] = function(callback) {
    // Push the given callback onto the list of functions to execute when ready.
    // If the dom has alredy loaded, call 'whenReady' right away.
    // Otherwise bind the ready-event if it hasn't been done already
    readyList.push(callback);
    if (document.readyState == "complete") {   
      whenReady();
    } else if (!isBound) {
      bindReady();
      isBound = true;
    }
  };
})();

onDomReady(function() {
  include_once('Model');
  include_once('Controller');

  Model.factory(_root_core_path.model);
});

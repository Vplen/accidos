/**
 * Class: _model
 *
 * @package _model
 * 
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 * @license GNU GPL 3.0 http://www.gnu.org/licenses/gpl.txt
 */
include_once('Event');
function _model(){
  //constructor
  this.init();
}

_model.prototype.vars             = {VAR_EVENTS: {} };
_model.prototype._classname       = 'Model';  

_model.prototype.set   = function (key, value){
  if ( 'undefined' == typeof this.vars[key] )
    console.log(this._classname + '::' + key + ' SET undefined parameter.');
  else this.vars[key] = value;
  return this;
};

_model.prototype.get   = function (key){
  if ( 'undefined' == typeof this.vars[key] )
    console.log(this._classname + '::' + key + ' GET undefined parameter.');
  return this.vars[key];
};

_model.prototype.isset = function (key){
  return ('undefined' != typeof this.vars[key]);
};

_model.prototype.ensure  = function(err, msg){
  if (err) console.log(this._classname + '::ensure > ' + msg);
};

_model.prototype.exception_ensure = function(){
  var cb = Array.prototype.shift.call(arguments);
  var ct = Array.prototype.shift.call(arguments);
  try{  
    cb.apply(ct, arguments);
  }
  catch(e){
    console.log(this._classname + '::' + e);
    console.log('<<<CONTEXT>>>');
    console.log(ct);
    console.log('<<<PARAMETERS>>>');
    console.log(arguments);
  }
}

_model.prototype.bind_events = function(event){
  if (this.isset('VAR_EVENTS')){
    var es = this.get('VAR_EVENTS');
    for (var i in es) {
      Event.bind(i, this['event_' + i], this, es[i]);
    }
  }
};

_model.prototype.init  = function(){

};

_model.prototype.factory = function( child_name ){
  var src                     = source('models/' + child_name);
  var F                       = new Function('model', src);
  var ret                     = function(){};
  var Temp                    = function(){};
  Temp.prototype              = this;
  ret.prototype               = new Temp;
  ret.prototype.ancestor      = this;
  ret.prototype.constructor   = this.constructor;
  ret.prototype.vars          = {VAR_EVENTS:{}};
  try{
    F(ret);
  }
  catch(e){
    console.log('Crash load model <' + child_name + '> with error:' + e.description);
    console.log(e);
  }
  ret.prototype._classname    = child_name;
  ret                         = new ret;
  this.ensure(!(ret instanceof _model), "The class <" + child_name + "> aren't implements <_model> class.");
  ret.bind_events();
  ret.init();
  return ret;
};

var Model = new _model;

/**
 * Class: Controller
 *
 * @package Controller
 *
 * 
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 * @license GNU GPL 3.0 http://www.gnu.org/licenses/gpl.txt
 */
include_once('View');
include_once('Event');

function _controller(){
  //constructor
}

_controller.prototype._event            = '';
_controller.prototype._template         = '';
_controller.prototype._view             = null;
_controller.prototype._before           = function(){
  return this.finalize();
};
_controller.prototype.finalize          = function(){};
_controller.prototype._after            = function(){
  return this.initialize();  
};
_controller.prototype.initialize        = function(){};
_controller.prototype._action           = function(){
  return this.do_action();
};
_controller.prototype.do_action         = function(){};

_controller.prototype._run              = function(view){
  var template    = null;
  if (('undefined' != typeof view) && (view instanceof _view)){
    this._view    = view;
    template      = view.get_filename();
    this._view.set_filename(this._template);
  }
  else{
    this._view    = View.factory(this._template);
  }
  Event.trigger(this._event, this._view, this);
  this._before();
  this._action();
  this._after();
  var ret = this._view.render();
  if ((this._view instanceof _view) && null != template){
    this._view.set_filename(template);
  }

  return ret;
};

_controller.prototype._ensure  = function(err, msg){
  if (err) console.log(this.constructor.name + '::ensure > ' + msg);
};

_controller.prototype.factory           = function(child_name){
  var src                     = source('controllers/' + child_name);
  var F                       = new Function('controller', src);
  var ret                     = function(){ };
  var Temp                    = function(){};
  Temp.prototype              = this;
  ret.prototype               = new Temp;
  ret.prototype.ancestor      = this;
  ret.prototype.constructor   = this.constructor;
  F(ret);
  ret.prototype._event        = child_name.toLowerCase();
  ret                         = new ret;
  this._ensure(!(ret instanceof this.constructor), "The class <" + child_name + "> aren't implements <" + this.constructor.name + "> class.");
  this._ensure(!(ret instanceof _controller), 'Error format controller <' + child_name + '>');
  return ret;
};

_controller.prototype.execute           = function(name){
  return this.factory(name)._run();
};

_controller.prototype.extend            = function(name){
  return this.factory(name)._run(this._view); 
};

var Controller = new _controller;
